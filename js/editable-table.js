(function ($) {
    $(document).on('click', '.editable-table td:not(.editing)', function () {
        $(this).addClass('editing');
        $(this).html('<input type="text" class="edit-field"value = "' + $(this).html() + '"/>')
    });

    $(document).on('keypress', 'input.edit-field', function (e) {
        if (e.which == 13) {
            var value = $(this).val(),
                container = $(this).parent();

            updateField(container, value);

        }
    });

    function updateField(container, value)
    {
        $.post( "/admin/config/content/issart_test2_contacts/ajax_upload/", {
            data: {
                'id': container.attr('data-id'),
                'value': value,
                'validate': container.index() >= 1 ? 1 : 0
            }
        }, function (data) {
            if (data['success']) {
                container.html(value);
                container.removeClass('editing');
            } else {
                alert(data['error']);
            }
        });
    }
})(jQuery);