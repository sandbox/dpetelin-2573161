<table class="sticky-enabled tableheader-processed sticky-table editable-table">
  <thead>
  <tr>
    <?php foreach ($header as $headerItem): ?>
      <th><?php echo $headerItem['data']; ?></th>
    <?php endforeach; ?>
  </tr>
  </thead>
  <tbody>
  <?php foreach ($rows as $row): ?>
    <tr>
      <?php foreach ($row as $item): ?>
        <td
          data-id="<?php echo $item['id']; ?>"><?php echo $item['data']; ?></td>
      <?php endforeach; ?>
    </tr>
  <?php endforeach; ?>
  </tbody>
</table>